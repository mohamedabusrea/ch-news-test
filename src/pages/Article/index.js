import React, {useEffect, useState} from 'react';
import './styles.scss';

const Article = ({location, history}) => {
  const [article, setArticle] = useState({});

  useEffect(() => {
    (location && location.state && location.state.article) ? setArticle(location.state.article) : history.push('/');
  }, []);
  return (<div className={'article'}>
    <img src={article.urlToImage}
         className={'article__img'}
         alt={article.title}/>
    <div className='article__body'>
      <h3 className='article__title'>{article.title}</h3>
      <p className='article__content'>{article.content}</p>
      <a href={article.url}
         target='_blank'
         rel='noopener noreferrer'
         className='article__link'>
        Read the full story
      </a>
    </div>
  </div>);
};

export default Article;