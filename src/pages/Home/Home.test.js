import React from 'react';
import Home from './index';
import {configure, mount, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

it('renders without crashing', () => {
  shallow(<Home/>);
});
it('Categories length should be 8', () => {
  const wrapper = mount(<Home/>);

  expect(wrapper.find('.header__listItem').length).toBe(8);
});
it('PreLoader should be on at the beginning with length 20', () => {
  const wrapper = mount(<Home/>);

  expect(wrapper.find('.loadingCircle').length).toBe(20);
});