import React, {useEffect, useState} from 'react';
import Header from '../../components/Header';
import LoadingCircle from '../../components/LoadingCircle';
import TopHeadlineRow from '../../components/TopHeadlineRow';

const Home = ({location}) => {
  const [topHeadlinesArray, setTopHeadlinesArray] = useState(Array.from(Array(20)));
  const [selectedCategoryIndex, setSelectedCategoryIndex] = useState(0);
  const [loadingCircleFlag, setLoadingCircleFlag] = useState(true);

  useEffect(() => {
    (location && (!location.state || !location.state.articles)) && getTopHeadlines();
  }, [selectedCategoryIndex]);

  const categoriesArray = ['all', 'business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology'];
  const onClickCategory = async (index) => {
    navigator.onLine ? await setSelectedCategoryIndex(index) : alert('Check your internet connection first');
  };
  const getTopHeadlines = async () => {
    const apiKey = 'bearer 9fe3f55af1ea4011ad1784da0c9a86c1';
    const selectedCategory = selectedCategoryIndex ? `&category=${categoriesArray[selectedCategoryIndex]}` : '';
    const url = `https://newsapi.org/v2/top-headlines?country=us${selectedCategory}`;

    setLoadingCircleFlag(true);
    try {
      const response = await fetch(url, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: apiKey,
        },
      });

      const data = await response.json();

      if (response.status !== 200) {
        alert(data.message);
      }
      else {
        setTopHeadlinesArray(data.articles);
        localStorage.setItem('articles', JSON.stringify({articles: data.articles, selectedCategoryIndex}));
      }

      location.state = {article: data.articles};
      setLoadingCircleFlag(false);
      console.log(data);
    }
    catch (error) {
      const localData = localStorage.getItem('articles');

      navigator.onLine ? alert('Failed to fetch data from the API. Make sure you enabled th CORS plugin.') :
          alert('Check your internet connection');

      if (localData) {
        const data = JSON.parse(localData);

        setTopHeadlinesArray(data.articles);
        setSelectedCategoryIndex(data.selectedCategoryIndex);
        setLoadingCircleFlag(false);
      }
    }
  };

  return (
      <div>
        <Header selectedCategoryIndex={selectedCategoryIndex}
                linksArray={categoriesArray}
                onClickLink={onClickCategory}/>
        <div className={'pageContent'}>
          <ul>
            {topHeadlinesArray.map((item, index) => loadingCircleFlag ?
                <LoadingCircle key={index}/> :
                <TopHeadlineRow item={item} key={index}/>)}
          </ul>
        </div>
      </div>);
};

export default Home;