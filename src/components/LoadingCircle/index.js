import React from 'react';
import ContentLoader from 'react-content-loader';

const LoadingCircle = () => {
  return (<div className='loadingCircle' style={{padding: '0 20px'}}>
    <ContentLoader style={{maxWidth: '400px'}}>
      <rect x="0" y="0" rx="5" ry="5" width="150" height="100"/>
      <rect x="170" y="17" rx="4" ry="4" width="230" height="13"/>
      <rect x="170" y="40" rx="3" ry="3" width="190" height="7"/>
      <rect x="170" y="55" rx="3" ry="3" width="140" height="7"/>
    </ContentLoader>
  </div>);
};

export default LoadingCircle;