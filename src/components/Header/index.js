import React from 'react';
import './styles.scss';

const Header = ({linksArray, selectedCategoryIndex, onClickLink}) => {
  return (<header className='header'>
    <ul className='header__list'>
      {linksArray.map((title, index) => <li className='header__listItem' key={index}>
        <a className={`header__link ${(selectedCategoryIndex === index) && 'header__link--active'}`}
           onClick={() => onClickLink(index)}>
          {title}
        </a>
      </li>)}
    </ul>
  </header>);
};

export default Header;