import React from 'react';
import './styles.scss';
import {Link} from 'react-router-dom';

const TopHeadlineRow = ({item}) => {
  return (item.urlToImage && <li className='headlineRow'>
    <Link to={{pathname: '/article', state: {article: item}}}>
      <img src={item.urlToImage}
           className={'headlineRow__img'}
           alt="top headline"/>
    </Link>
    <div className={'headlineRow__textContent'}>
      <Link to={{pathname: '/article', state: {article: item}}}>
        <h3 className={'headlineRow__title'}>{item.title}</h3>
      </Link>

      <p className={'headlineRow__description'}>{item.description}</p>
    </div>
  </li>);
};

export default TopHeadlineRow;