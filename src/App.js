import React, {Component} from 'react';
import Home from './pages/Home';
import Article from './pages/Article';
import './App.scss';
import {BrowserRouter as Router, Route} from 'react-router-dom';

class App extends Component {
  render() {
    return (
        <Router>
          <div className="App">
            <div className="container">
              <Route exact path="/" component={Home}/>
              <Route exact path="/article" component={Article}/>
            </div>
          </div>
        </Router>
    );
  }
}

export default App;
