## Installation
#####Development
 - Clone the repo `git clone https://mohamedabusrea@bitbucket.org/mohamedabusrea/ch-news-test.git`
 - Go to the folder `cd ch-news-test`
 - Install the dependencies `npm i`
 - Install this plugin for chrome [Allow-Control-Allow-Origin](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi)
 - make sure to enable the "CORS" plugin
 - Start the development server `npm run start`
 - To run the tests `npm run test`
 
#####Production (PWA features)
 - To test the PWA features you need to run it on the production:
    - Build the project `npm run build`
    - Install serve library `npm i serve -g`
    - Serve the build folder with a static server `serve build`
    - now you can test all PWA features and add a shortcut for your project
 
## Introduction
 I really had fun working in this test and tried my best to finish as much as I can in the little time I had (this week was very busy at work). This test reflects the mindset of the team and how you're trying to keep up with the new tools/frameworks and following the best practices which I really appreciate.
 
## First Challenge 

On the first challenge, we need to 

- Create the account in https://newsapi.org ---- **Done**
- Create a boilerplate project ---- **Done**
- Show the list of categories in the homepage ---- **Done**
- Show the headlines on the homepage ---- **Done**
- Show the news filtered by categories on a new page ---- **Done**

#### Considerations
- Need to be a Single Page Application ---- **Done**
- Need to be PWA ---- **Done**
- Show images ---- **Done** 


## Second Challenge
 
we should be able to 

- Access offline to a previously visited content ---- **Done**
- Have a proper Manifest in place ---- **Done**
- Cache all the static content ---- **Done**


## Third Challenge

Instead of waiting to have all the content before rendering the page we should apply progressive loading. **Done** 

**Note:** 
- I used the facebook placeholder style to enhance the user's experience.
- I was going to lazy load tha images too but didn't have much time.

## Forth Challenge

Add at least 1 micro-interaction. ---- **Not Finished**

**Note**: couldn't come up with a creative one because of the tight time so I gave the test the priority 


## Fifth Challenge

Test all the things. We are going to focus on functional tests. **partially Done** 
 